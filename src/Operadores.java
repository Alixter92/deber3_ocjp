/**
 * Created by USER on 05/06/2017.
 */
public class Operadores {

    public static void main(String[] args) {

        //operadores
        int i = 7;
        float f = 5.5F;
        char c = 'w';
        float resultado;
        String comprobar;
        //operadores Unarios
        System.out.println("i comienza con el valor de:"+i);
        //el resultado es el mismo ya que el valor primero evalua la variable antes de imprimir;
        resultado= i++;
        System.out.println("el resultado de 'i++' es:"+resultado);
        System.out.println("i ahora es es:"+i);
        //como en el caso anterior en la primera impresion nos da el valor de i antes de realizar la operacion
        resultado= i--;
        System.out.println("el resultado de 'i--' es:"+resultado);
        System.out.println("i ahora es es:"+i);
        //cuando se pone adelante de la variable los signos se realiza la oepracion antes de evaluar el tipo de variable
        resultado= ++i;
        System.out.println("el resultado de '++i' es:"+resultado);
        System.out.println("i ahora es es:"+i);
        //el mismo caso que las sentencias anteriores
        resultado= --i;
        System.out.println("el resultado de '--i' es:"+resultado);
        System.out.println("i ahora es es:"+i);

        /////////////////////////////////////

        ///operadores de comparacion

        if ((i >= 6) && (c == 'w')){
            comprobar="verdadero";
            System.out.println("la expresion (i >= 6) && (c == 'w') es : "+comprobar+" ya que"+i+" y "+c);
        }else {
            comprobar="falso";
            System.out.println("la expresion i>=6&&c==W es : "+comprobar+" ya que "+i+" y "+c);
        }

        if ((c == 'p') || (i + f <= 10)){
            comprobar="verdadero";
            System.out.println("la expresion (c == 'p') || (i + f <= 10) es : "+comprobar+" ya que "+c+" y "+(i+f));
        }else {
            comprobar="falso";
            System.out.println("la expresion (c == 'p') || (i + f <= 10) es : "+comprobar+" ya que "+c+" y "+(i+f));
        }

        if ((f < 11) && (i > 100)){
            comprobar="verdadero";
            System.out.println("la expresion (f < 11) && (i > 100) es: "+comprobar+" ya que "+f+" y "+i);
        }else {
            comprobar="falso";
            System.out.println("la expresion (f < 11) && (i > 100) es: "+comprobar+" ya que "+f+" y "+i);
        }



    }
}
