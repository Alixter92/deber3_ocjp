/**
 * Created by USER on 05/06/2017.
 */
public class datosPrimitivos {

        //declaramos las variables de tipo primitivo que usaremos

        boolean t;
        char c;
        byte b;
        short s;
        int i;
        long l;
        float f;
        double d;

    void imprimirValoresIniciales() {

        //creamos una funcion para imprimir los valores por defecto del programa
        System.out.print("Tipo Variable\tValor Inicial");
        System.out.println("boolean" + t);
        System.out.println("char[" + c + "]");
        System.out.println("byte" + b);
        System.out.println("short" + s);
        System.out.println("int" + i);
        System.out.println("long" + l);
        System.out.println("float" + f);
        System.out.println("double" + d);

    }

}

