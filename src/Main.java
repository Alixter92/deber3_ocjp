public class Main {

    public static void main(String[] args) {

        //instanciamos la clase datosPrimitivos
        //y llamamos a la funcion para imprimir los datos
        datosPrimitivos datos = new datosPrimitivos();
        datos.imprimirValoresIniciales();



        int a=1;
        double b=2.5;
        Object o = "Hello";
        String s;

        ///////////CASTING//////////////
        /// a=b; CON ESTA SENTECIA NOS DARIA UN ERROR ya que un int no puede contener a un booleano
        //para solucionar esto se realiza el casting para asginar el tipo de dato que queremos pasar
        //en este caso (int)
        a=(int)b;
        System.out.println(a);
        //Un casting de un objeto a String, se realiza esto ya que un String no puede contener a un Objeto

        s = (String)o;
        System.out.println(s);

        //////CONVERSION/////
        ///ESTA CONVERSION ES de un tipo numerico a un tipo string
        //mediante el .toString()
        Float a1 = new Float("3.45");

        String b1= new String("");

        b1 = a1.toString();
        System.out.println(b1);

        ////conversion de String a un objeto numerico Double
        //mediante valueOf()

        String a2=new String("50");

        Double b2=new Double(0);

        b2 = b2.valueOf(a2);

    }
}
